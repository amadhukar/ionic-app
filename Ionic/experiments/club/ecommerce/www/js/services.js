angular.module('ecommerce.services', [])

.service("authService",function($http,$q,$log){

  $log.log("Here in the service "); 

  var user_token = ""; 

  //gets the user token
  this.getUserToken = function(){
    return user_token; 
  }

  //logs the user in
  this.login = function(user){
    var deferred = $q.defer(); 
    var credentials={
      email:user.email, 
      password:user.password
    }

    $http.post('http://ecommercedev.azurewebsites.net/auth/login',credentials).success(function(data,status,headers,config){

      $log.log("http success "+status); 
      $log.log("userid: "+data.user_token);
      user_token=data.user_token;
      deferred.resolve(data); 

    }).error(function(data,status,headers,config){
      $log.log("http failure "+status); 
      $log.log("message "+data.message); 
      deferred.reject(data); 
    }); 

    return deferred.promise;
  }

  this.signup = function(user){
    var deferred = $q.defer(); 
    var credentials = {
      'email':user.email, 
      'password':user.password
    }; 
    $log.log("Here in the signup function");


    $http.post('http://ecommercedev.azurewebsites.net/auth/signup',credentials).success(function(data,status,headers,config){

       $log.log("Status Code: "+status);
       $log.log("data: "+data.message);
       deferred.resolve(data); 

    }).error(function(data,status,headers,config){
        deferred.reject(data); 
    }); 

    return deferred.promise;
  }

})

.service("homeService",function($http,$log,$q){
    $log.log("Here in the homeService"); 

    this.getCategories = function(){
      var deferred = $q.defer(); 
      $log.log("here in getCategories function"); 

      $http.get('http://ecommercedev.azurewebsites.net/category').success(function(data,status,headers,config){
        $log.log("here in success"); 
        $log.log("status: "+status);
        deferred.resolve(data);  
      }).error(function(data,status,headers,config){
        $log.log("here in failure"); 
        $log.log("status: "+status); 
        $log.log("message: "+data.message); 
        deferred.reject(data); 
      }); 

      return deferred.promise; 
    }
})