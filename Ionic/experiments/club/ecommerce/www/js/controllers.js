angular.module('ecommerce.controllers', [])

//to login to the app 
.controller("loginCtrl",function($log,$scope,authService,$ionicPopup,$state){

    $log.log("here in the login Ctrl"); 
    $scope.user={}; 

    $scope.login = function(){
      $log.log("login function ");
      $log.log("email: "+$scope.user.email);
      $log.log("password: "+$scope.user.password); 
      authService.login($scope.user).then(function(data){
        $log.log("call login service "+data.user_token); 
        $scope.user.email = ""; 
        $scope.user.password=""; 
        $ionicPopup.alert({
          title:'Login',
          template:'You have successfully logged in'
        }).then(function(res){
          $log.log("redirect to home page"); 
          $state.go("home"); 
        }); 
       
      },function(error){
         $ionicPopup.alert({
            title:'Login Failed',
            template:'You can re-enter your credentials'
          }).then(function(res){
            $log.log("login failed."); 
          });
      });   
    }
})


//to sign up for the app
.controller("signupCtrl",function($log,$scope,authService,$ionicPopup,$state){
  $log.log("Here in the sign up Ctrl");
  $scope.user={}; 
  $scope.signup = function(){
    $log.log("Signup function");
    $log.log("email: "+$scope.user.email);
    $log.log("email: "+$scope.user.password);

    if ($scope.user.password === $scope.user.confirmPassword) {

      authService.signup($scope.user).then(function(data){
          $ionicPopup.alert({
            title:'Sign Up',
            template:'New Account Created!'
          }).then(function(res){
            $log.log("new account created popup"); 
            $scope.user={}; 
            $state.go("login"); 
          }); 
      },function(error){
          $log.log("Failed to sign up error: "+error.message); 
          if (error.message!=="") {
            $ionicPopup.alert({
              title:'Sign Up Failed',
              template:error.message
            }).then(function(res){
              $log.log("Sign Up failed popup");
              $scope.user={};  
            });
          };
      }); 

    }else{
      $ionicPopup.alert({
        title:'Sign Up Failed',
        template:'Passwords don\' match'
      }).then(function(res){
         $log.log("passwords's don't match popup");
         $scope.user = {};  
      });
    }     
  } 
})

.controller("homeCtrl",function($log,$scope,homeService,$ionicPopup){
    $log.log("Here in the home Ctrl"); 

    $scope.products = []; 
    $scope.categories=[
      /*  {'id':1, 'name':'footwear','description':'shoes'},
        {'id':2, 'name':'shirts','description':'fun shirts'},
        {'id':3,'name':'hats','description':'cover your head'} */
    ]; 

    $scope.showCategoryDelete=false;

    $scope.loadCategories = function(){
        homeService.getCategories().then(function(data){
          $scope.categories=data.data;
        },function(error){
            $ionicPopup.alert({
              title:'Failure',
              template:error.message
            }).then(function(res){
              $log.log("failed to load data"); 
            });
        }); 
    }

    $scope.loadCategories(); 

})
